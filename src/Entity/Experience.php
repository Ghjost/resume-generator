<?php

namespace App\Entity;

use App\Repository\ExperienceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: ExperienceRepository::class)]

/**
 * @ORM\Entity(repositoryClass=ExperienceRepository::class)
 */
class Experience
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $publicId;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $company;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $job;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $startDate;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $endDate;

    #[ORM\ManyToOne(targetEntity: Resume::class, inversedBy: 'experiences')]
    private $resume;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $inProgress;

    public function __construct()
    {
        $this->publicId = 'EXP_' . strval(new Ulid());
        $this->inProgress = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublicId(): ?string
    {
        return $this->publicId;
    }

    public function setPublicId(?string $publicId): self
    {
        $this->publicId = $publicId;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(?string $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getResume(): ?Resume
    {
        return $this->resume;
    }

    public function setResume(?Resume $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getInProgress(): ?bool
    {
        return $this->inProgress;
    }

    public function setInProgress(?bool $inProgress): self
    {
        $this->inProgress = $inProgress;

        return $this;
    }
}
