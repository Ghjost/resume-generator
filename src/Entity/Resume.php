<?php

namespace App\Entity;

use App\Repository\ResumeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: ResumeRepository::class)]

/**
 * @ORM\Entity(repositoryClass=ResumeRepository::class)
 */
class Resume
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $publicId;

    // #[ORM\OneToMany(mappedBy:'resume', targetEntity:Experience::class, cascade:['persist'])]
    // private $experiences;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $resumeTitle;

    #[ORM\OneToMany(mappedBy: 'resume', targetEntity: Diploma::class, orphanRemoval: true, cascade: ['persist'])]
    #[Groups(['resume:item:read'])]
    private $diplomas;

    #[ORM\OneToMany(mappedBy: 'resume', targetEntity: Experience::class, orphanRemoval: true, cascade: ['persist'])]
    #[Groups(['resume:item:read'])]
    private $experience;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'resume')]
    private $user;

    public function __construct()
    {
        $this->publicId = 'R_' . strval(new Ulid());
        $this->diplomas = new ArrayCollection();
        $this->experience = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublicId(): ?string
    {
        return $this->publicId;
    }

    public function setPublicId(?string $publicId): self
    {
        $this->publicId = $publicId;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getResumeTitle(): ?string
    {
        return $this->resumeTitle;
    }

    public function setResumeTitle(?string $resumeTitle): self
    {
        $this->resumeTitle = $resumeTitle;

        return $this;
    }

    /**
     * @return Collection<int, Diploma>
     */
    public function getDiplomas(): Collection
    {
        return $this->diplomas;
    }

    public function addDiploma(Diploma $diploma): self
    {
        if (!$this->diplomas->contains($diploma)) {
            $this->diplomas[] = $diploma;
            $diploma->setResume($this);
        }

        return $this;
    }

    public function removeDiploma(Diploma $diploma): self
    {
        if ($this->diplomas->removeElement($diploma)) {
            // set the owning side to null (unless already changed)
            if ($diploma->getResume() === $this) {
                $diploma->setResume(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Experience>
     */
    public function getExperience(): Collection
    {
        return $this->experience;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experience->contains($experience)) {
            $this->experience[] = $experience;
            $experience->setResume($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experience->removeElement($experience)) {
            // set the owning side to null (unless already changed)
            if ($experience->getResume() === $this) {
                $experience->setResume(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
