<?php

namespace App\Entity;

use App\Repository\DiplomaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: DiplomaRepository::class)]

/**
 * @ORM\Entity(repositoryClass=DiplomaRepository::class)
 */
class Diploma
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $publicId;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $speciality;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $date;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $obtained;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['resume:item:read'])]
    private $location;

    #[ORM\ManyToOne(targetEntity: Resume::class, inversedBy: 'diplomas')]
    #[ORM\JoinColumn(nullable: false)]
    private $resume;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['resume:item:read'])]
    private $inProgress;

    public function __construct()
    {
        $this->publicId = 'DIP_' . strval(new Ulid());
        $this->obtained = false;
        $this->inProgress = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublicId(): ?string
    {
        return $this->publicId;
    }

    public function setPublicId(?string $publicId): self
    {
        $this->publicId = $publicId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(?string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getObtained(): ?bool
    {
        return $this->obtained;
    }

    public function setObtained(?bool $obtained): self
    {
        $this->obtained = $obtained;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getResume(): ?Resume
    {
        return $this->resume;
    }

    public function setResume(?Resume $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getInProgress(): ?bool
    {
        return $this->inProgress;
    }

    public function setInProgress(?bool $inProgress): self
    {
        $this->inProgress = $inProgress;

        return $this;
    }
}
