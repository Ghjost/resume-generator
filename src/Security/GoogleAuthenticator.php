<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class GoogleAuthenticator extends AbstractAuthenticator
// class GoogleAuthenticator extends SocialAuthenticator
{
    private $clientRegistry;
    private $emi;
    private $router;
    private $userRepository;
    private $urlGenerator;

    public function __construct(
        ClientRegistry $clientRegistry,
        EntityManagerInterface $emi,
        RouterInterface $router,
        UserRepository $userRepository,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->clientRegistry = $clientRegistry;
        $this->emi = $emi;
        $this->userRepository = $userRepository;
        $this->urlGenerator = $urlGenerator;
    }

    public function supports(Request $request): ?bool
    {
        // TODO: Implement supports() method.
        return 'app_login_google_check' === $request->attributes->get('_route');
    }

    public function authenticate(Request $request): Passport
    {
        $client = $this->clientRegistry->getClient('google_main');
        // dd($client);
        $accessToken = $client->getAccessToken();

        // dd($accessToken);

        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), function () use ($accessToken, $client) {
                /** @var GoogleUser $googleUser */
                $googleUser = $client->fetchUserFromToken($accessToken);

                // "getAvatar", "getEmail", "getFirstName", "getHostedDomain", "getId", "getLastName", "getLocale" or "getName"

                // dd($this->getGoogleClient()->fetchUserFromToken($accessToken), $this->getGoogleClient()->fetchUserFromToken($accessToken)->getAvatar());

                $email = $googleUser->getEmail();

                // dd($email);
                // 1) have they logged in with Facebook before? Easy!
                $existingUser = $this->userRepository->findOneBy(['email' => $email]);

                if ($existingUser) {
                    return $existingUser;
                }

                // 2) do we have a matching user by email?
                // $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

                $user = new User();
                // $user = $this->userRepository->findOneBy(['email' => $email]);
                $user->setEmail($email);
                $user->setFirstName($googleUser->getFirstName());
                $user->setLastName($googleUser->getLastName());
                $user->setAvatar($googleUser->getAvatar());
                $user->setEmail($email);
                $user->setPassword(null);

                $this->emi->persist($user);
                $this->emi->flush();
                // 3) Maybe you just want to "register" them by creating
                // a User object
                // $user->setFacebookId($googleUser->getId());
                // $this->entityManager->persist($user);
                // $this->entityManager->flush();

                return $user;
            })
        );
    }

    /**
     * @return GoogleClient
     */
    private function getGoogleClient()
    {
        return $this->clientRegistry
            // "facebook_main" is the key used in config/packages/knpu_oauth2_client.yaml
            ->getClient('google_main');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // TODO: Implement onAuthenticationSuccess() method.
        // return new Response('Ok');
        // $targetUrl = $this->router->generate('app_home');

        // return new RedirectResponse($targetUrl);
        return new RedirectResponse($this->urlGenerator->generate('app_home'));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        // TODO: Implement onAuthenticationFailure() method.
        // return new Response('Pas ok');
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

//    public function start(Request $request, AuthenticationException $authException = null): Response
//    {
//        /*
//         * If you would like this class to control what happens when an anonymous user accesses a
//         * protected page (e.g. redirect to /login), uncomment this method and make this class
//         * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface.
//         *
//         * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
//         */
//    }
}
