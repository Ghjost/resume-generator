<?php

namespace App\Controller\User;

use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UserController extends AbstractController
{
    public function __construct(
        private NormalizerInterface $normalizerInterface,
        private EntityManagerInterface $emi
        // private Request $request
    ) {
    }

    #[Route('/me', name: 'app_user_me')]
    public function getMe(): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        $form = $this->createForm(UserType::class);

        return $this->render('user/profile.html.twig', [
        'page_title' => 'Mon profil',
        'form' => $form->createView(),
        'user' => $this->normalizerInterface->normalize($this->getUser(), null, ['groups' => ['user:item:read']]),
        ]);
    }

    #[Route('/me/edit', name: 'app_user_edit')]
    public function edit(Request $request): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        $form = $this->createForm(UserType::class, $this->getUser())->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->emi->persist($this->getUser());
            $this->emi->flush();

            $this->addFlash(
                'updated',
                'Votre profil a bien été mis à jour !'
            );

            return $this->redirectToRoute('app_user_me');
        }

        return $this->render('user/editProfile.html.twig', [
        'page_title' => 'Edition profil',
        'form' => $form->createView(),
        'user' => $this->normalizerInterface->normalize($this->getUser(), null, ['groups' => ['user:item:read']]),
        ]);
    }
}
