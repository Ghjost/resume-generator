<?php

namespace App\Controller\Login;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GoogleController extends AbstractController
{
    #[Route('/login/google', name: 'app_login_google')]
    public function loginRedirect(ClientRegistry $clientRegistry): RedirectResponse
    {
        // Other possible options
        // return $clientRegistry->getClient('google_main')->redirect([], ['prompt' => 'none']);
        // return $clientRegistry->getClient('google_main')->redirect([], ['prompt' => 'consent']);
        return $clientRegistry->getClient('google_main')->redirect([], ['prompt' => 'select_account']);
    }

    #[Route('/login/google/check', name: 'app_login_google_check')]
    public function loginRedirectCheck(ClientRegistry $clientRegistry): Response
    {
        // Use authenticators to authenticate the User in firewall
        // $client = $clientRegistry->getClient('google_main');
        // return $this->json([
        //     'user' => $client->fetchUserFromToken($client->getAccessToken()),
        // ]);

        return $this->redirectToRoute('app_home');
    }
}
