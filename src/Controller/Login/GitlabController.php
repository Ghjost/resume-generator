<?php

namespace App\Controller\Login;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GitlabController extends AbstractController
{
    #[Route('/login/gitlab', name: 'app_login_gitlab')]
    public function loginRedirect(): Response
    {
        return $this->json([
        'message' => 'Welcome to your new controller!',
        'path' => 'src/Controller/Login/GitlabController.php',
        ]);
    }

    #[Route('/login/gitlab/check', name: 'app_login_gitlab_check')]
    public function loginRedirectCheck(): Response
    {
        return $this->json([
        'message' => 'Welcome to your new controller!',
        'path' => 'src/Controller/Login/GitlabController.php',
        ]);
    }
}
