<?php

namespace App\Controller\Generator\CV;

use App\Entity\Resume;
use App\Form\ResumeType;
use App\Repository\ResumeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class HomeController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $emi
    ) {
    }

    #[Route('/', name:'app_home')]
    public function index(Request $request): Response
    {
        return $this->redirectToRoute('app_generator_cv_new_see_all');
    }

    #[Route('/generator/cv/new', name:'app_generator_cv_new')]
    public function newResume(Request $request): Response
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $resume = new Resume();
        $resume->setFirstName($currentUser->getFirstName());
        $resume->setLastName($currentUser->getLastName());
        $currentUser->addResume($resume);

        $resumeForm = $this->createForm(ResumeType::class, $resume)->handleRequest($request);

        if ($resumeForm->isSubmitted()) {
            dump($resume);

            $this->emi->persist($resume);
            $this->emi->flush();

            $this->addFlash(
                'created',
                'Votre nouveau CV a bien été crée !'
            );

            return $this->redirectToRoute('app_generator_cv_new_see_all');
        }

        return $this->render('generator/cv/index/newResume.html.twig', [
        'controller_name' => 'HomeController',
        'active' => 'new',
        'form' => $resumeForm->createView(),
        'title_page' => 'Nouveau CV',
        ]);
    }

    #[Route('/generator/cv/edit/{publicId}', name:'app_generator_cv_edit')]
    public function editResume(Request $request, string $publicId, resumeRepository $resumeRepository): Response
    {
        $resume = $resumeRepository->findOneBy(['publicId' => $publicId]);

        $resumeForm = $this->createForm(ResumeType::class, $resume)->handleRequest($request);

        if ($resumeForm->isSubmitted()) {
            dump($resume);

            $this->emi->persist($resume);
            $this->emi->flush();

            $this->addFlash(
                'updated',
                'Votre CV a bien été mis à jour !'
            );

            return $this->redirectToRoute('app_generator_cv_new_see_all');
        }

        return $this->render('generator/cv/index/newResume.html.twig', [
        'controller_name' => 'HomeController',
        'active' => 'new',
        'form' => $resumeForm->createView(),
        'title_page' => 'Edition ' . $resume->getResumeTitle(),
        ]);
    }

    #[Route('/generator/cv/remove/{publicId}', name:'app_generator_cv_remove')]
    public function removeResume(Request $request, string $publicId, resumeRepository $resumeRepository): Response
    {
        $resume = $resumeRepository->findOneBy(['publicId' => $publicId]);

        $this->emi->remove($resume);
        $this->emi->flush();

        $this->addFlash(
            'removed',
            'Votre CV a bien été supprimé'
        );

        return $this->redirectToRoute('app_generator_cv_new_see_all');
    }

    #[Route('/generator/cv/see/all', name:'app_generator_cv_new_see_all')]
    public function seeAllResume(Request $request, ResumeRepository $resumeRepository): Response
    {
        // $currentUser = $this->getUser();

        // dd($currentUser->getResume());
        $allResume = $resumeRepository->findBy(['user' => $this->getUser()]);

        dump($allResume);

        return $this->render('generator/cv/index/seeAllResume.html.twig', [
        'controller_name' => 'HomeController',
        'active' => 'seeAllResume',
        'resume_list' => $allResume,
        'title_page' => 'Mes CV',
        ]);
    }

    #[Route('/generator/cv/see/{publicId}', name:'app_generator_cv_see_one')]
    public function seeOneResume(Request $request, string $publicId, ResumeRepository $resumeRepository, NormalizerInterface $normalizerInterface): Response
    {
        $resume = $resumeRepository->findOneBy(['publicId' => $publicId]);

        return $this->render('generator/cv/index/seeOneResume.html.twig', [
        'controller_name' => 'HomeController',
        'active' => 'seeAllResume',
        'resume' => $normalizerInterface->normalize($resume, null, ['groups' => ['resume:item:read']]),
        'title_page' => 'CV ' . $resume->getResumeTitle(),
        ]);
    }
}
