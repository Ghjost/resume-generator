<?php

namespace App\Controller\Generator\CV;

use App\Repository\ResumeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ExportController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $emi,
        // private Request $request,
        private ResumeRepository $resumeRepository,
        private NormalizerInterface $normalizerInterface,
        private SluggerInterface $sluggerInterface
    ) {
    }

    #[Route('/generator/cv/export/{publicId}/json', name: 'app_generator_cv_export_json')]
    public function exportJson(string $publicId): Response
    {
        // return $this->redirectToRoute('app_generator_cv_new_see_all');
        $resume = $this->resumeRepository->findOneBy(['publicId' => $publicId]);

        return $this->json($this->normalizerInterface->normalize($resume, null, ['groups' => ['resume:item:read']]));
    }

    #[Route('/generator/cv/export/{publicId}/pdf/{view}', name: 'app_generator_cv_export_pdf')]
    public function exportPdf(
        string $publicId,
        Pdf $pdf,
        ?string $view = null,
        Request $request
    ) {
        $rootDir = $this->getParameter('kernel.project_dir') . '/public/' . $request->getBasePath();

        $resume = $this->resumeRepository->findOneBy(['publicId' => $publicId]);

        $html = $this->renderView('generator/cv/export/seeOneResume.html.twig', [
        'resume' => $this->normalizerInterface->normalize($resume, null, ['groups' => ['resume:item:read']]),
        'user' => $this->normalizerInterface->normalize($this->getUser(), null, ['groups' => ['user:item:read']]),
        ]);

        dump($rootDir);

        if ('view' == $view) {
            return $this->render('generator/cv/export/seeOneResume.html.twig', [
            'resume' => $this->normalizerInterface->normalize($resume, null, ['groups' => ['resume:item:read']]),
            'user' => $this->normalizerInterface->normalize($this->getUser(), null, ['groups' => ['user:item:read']]),
            ]);
        }

        return new PdfResponse(
            $pdf->getOutputFromHtml(
                $html,
                [
                'enable-local-file-access' => true,
                // 'page-size' => 'A4',
                'page-height' => 297,
                'page-width' => 210,
                // 'images' => true,
                // 'toc' => true,
                'no-images' => false,
                // 'footer-center' => 'Oui',
                'footer-font-size' => 5,
                // 'footer-html' => 'Generated with Resume Generator and &hearts; By Julien Drieu',
                // 'footer-left' => null,
                // 'footer-line' => null,
                // 'no-footer-line' => null,
                'footer-right' => 'Generated with Resume Generator By resume-generator.juliendrieu.fr',
                'print-media-type' => true,
                // 'no-background' => false,
                // 'footer-spacing' => null,
                // 'header-center' => null,
                // 'header-font-name' => null,
                // 'header-font-size' => null,
                // 'header-html' => null,
                // 'header-left' => null,
                // 'header-line' => null,
                // 'no-header-line' => null,
                // 'header-right' => null,
                ]
            ),
            $this->sluggerInterface->slug($resume->getResumeTitle() . ' ' . $resume->getFirstName() . ' ' . $resume->getLastName(), '_') . '.pdf'
        );

        // return $this->render('generator/cv/export/seeOneResume.html.twig', [
        //     'resume' => $this->normalizerInterface->normalize($resume, null, ['groups' => ['resume:item:read']]),
        // ]);
    }
}
