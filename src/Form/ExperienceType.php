<?php

namespace App\Form;

use App\Entity\Experience;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('company', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom de l\'entreprise',
                ],
            ])
            ->add('job', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Job dans l\'entreprise',
                ],
            ])
            ->add('startDate', DateType::class, [
                'label' => 'Date de début de l\'experience',
                'attr' => [
                    'class' => 'form-control',
                ],
                'years' => range(date('Y'), date('Y') - 100),
                'widget' => 'single_text',
            ])
            ->add('inProgress', CheckboxType::class, [
                'label' => 'Toujours en experience',
                'attr' => [
                    'class' => 'form-control',
                    // 'placeholder' => 'Description',
                ],
                'required' => false,
                // 'mapped' => false,
                // 'data' => false,
            ])
            ->add('endDate', DateType::class, [
                'label' => 'Date de fin de l\'experience',
                'attr' => [
                    'class' => 'form-control',
                ],
                'years' => range(date('Y'), date('Y') - 100),
                'widget' => 'single_text',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Experience::class,
        ]);
    }
}
