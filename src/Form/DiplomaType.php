<?php

namespace App\Form;

use App\Entity\Diploma;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DiplomaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nom ou titre du diplome',
                ],
                // "empty_data" => "",
            ])
            ->add('speciality', TextType::class, [
                'label' => false,
                // 'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Option du titre/diplome',
                ],
                // "empty_data" => "",
            ])
            ->add('inProgress', CheckboxType::class, [
                'label' => 'En attente d\'obtention ?',
                'attr' => [
                    'class' => 'form-control',
                    // 'placeholder' => 'Description',
                ],
                'required' => false,
                // 'mapped' => false,
                // 'data' => false,
            ])
            ->add('date', DateType::class, [
                'label' => 'Date d\'obtention du diplome/titre',
                'attr' => [
                    'class' => 'form-control',
                ],
                'data' => new \DateTime('now'),
                // 'choices' => range(date('Y') - 90, date('Y')),
                // 'widget' => 'choices',
                'years' => range(date('Y'), date('Y') - 100),
                'widget' => 'single_text',
            ])

            ->add('obtained', CheckboxType::class, [
                'label' => 'Diplome/titre obtenus ?',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
                // "empty_data" => "0",
            ])
            ->add('location', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Localisation (Ex : MDS Rennes / Openclassroom ...)',
                ],
                'empty_data' => '',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Diploma::class,
        ]);
    }
}
