# Resume Generator

[![pipeline status](https://gitlab.com/Ghjost/resume-generator/badges/master/pipeline.svg)](https://gitlab.com/Ghjost/resume-generator/-/commits/master)

## The projet

ResumeGenerator is a project set up as part of a search for skills in the following areas:

- Use of form collection (with dynamic addition of fields)
- Use of a connection through a third-party service (Google, Linkedin, Apple, Facebook)
- Generation of PDF files

## Required skills

- Symfony Collection (CollectionType)
- Security component (Social login)

## External Bundle

- Social Login : [ knpuniversity /oauth2-client-bundle ](https://github.com/knpuniversity/oauth2-client-bundle)
- PDF export : [ KnpLabs / KnpSnappyBundle ](https://github.com/KnpLabs/KnpSnappyBundle)

## Architecture of database

![database architecture](https://i.imgur.com/dnw7Bpa.png)

## Functionnality

### Social Login

![Login choice panel](https://i.imgur.com/Vv5kezi.png)

![Login with Google](https://i.imgur.com/YXRRjvz.png)

### View all private resume

![View all resumes](https://i.imgur.com/nwmNrTw.png)

### View one private resume

![View all resumes](https://i.imgur.com/2q6be0g.jpg)

### Create new Resume

![Create new resume](https://i.imgur.com/GVJtxPj.png)

### Edit one resume

![View all resumes](https://i.imgur.com/Of6tY0G.png)

### Export Resume

#### PDF export

![Export PDF resume](https://i.imgur.com/HORU8IR.png)

#### JSON export

![Export JSON resume](https://i.imgur.com/sZ6JeNB.png)

### User profile

#### View my profile

![View my profile](https://i.imgur.com/NQNRjcF.png)

#### Edit my profile

![Edit my profile](https://i.imgur.com/QysHt8c.png)
