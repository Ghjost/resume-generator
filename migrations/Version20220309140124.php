<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309140124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE diploma ADD speciality VARCHAR(255) DEFAULT NULL, ADD date DATE DEFAULT NULL, ADD obtained TINYINT(1) DEFAULT NULL, ADD location VARCHAR(255) DEFAULT NULL, CHANGE d_option public_id VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE diploma ADD d_option VARCHAR(255) DEFAULT NULL, DROP public_id, DROP speciality, DROP date, DROP obtained, DROP location');
    }
}
