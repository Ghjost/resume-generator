<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308132243 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE diploma ADD resume_id INT NOT NULL');
        $this->addSql('ALTER TABLE diploma ADD CONSTRAINT FK_EC218957D262AF09 FOREIGN KEY (resume_id) REFERENCES resume (id)');
        $this->addSql('CREATE INDEX IDX_EC218957D262AF09 ON diploma (resume_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE diploma DROP FOREIGN KEY FK_EC218957D262AF09');
        $this->addSql('DROP INDEX IDX_EC218957D262AF09 ON diploma');
        $this->addSql('ALTER TABLE diploma DROP resume_id');
    }
}
