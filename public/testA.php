<?php
echo is_twin('Boob', 'obboa') == true ? 'Oui' : 'Non';
echo '<br>';
echo is_twin('Boob', 'obbo') == true ? 'Oui' : 'Non';
echo '<br>';
echo is_twin('BOB', 'obb') == true ? 'Oui' : 'Non';
echo '<br>';
echo is_twin('BaB', 'obb') == true ? 'Oui' : 'Non';

function is_twin(string $a, string $b)
{
    if (strlen($a) != strlen($b)) {
        return false;
    } else {
        $aSplit = str_split(strtolower($a));
        $bSplit = str_split(strtolower($b));

        sort($aSplit);
        sort($bSplit);

        if ($aSplit == $bSplit) {
            return true;
        } else {
            return false;
        }
    }
}
